﻿using FileUploadApi.Requests;
using FileUploadApi.Response;

namespace FileUploadApi.Interfaces
{
    public interface IPostService
    {
        //Task SavePostImageAsync(PostRequest postRequest);
        //Task<PostResponse>CreatePostAsync(PostRequest postRequest);

        //Task SavePostImageAsync1(RawPost rawPost);

        //Task<PostResponse>CreateSaveData(RawPost rawPost, PostRequest postRequest);


        Task SavePostImageAsync(RawPost rawPost);
        Task<PostResponse> CreatePostAsync(RawPost rawPost);

    }
}