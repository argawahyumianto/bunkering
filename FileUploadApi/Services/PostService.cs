﻿using FileUploadApi.Entities;
using FileUploadApi.Helpers;
using FileUploadApi.Interfaces;
using FileUploadApi.Requests;
using FileUploadApi.Response;
using FileUploadApi.Response.Models;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System;
using Microsoft.Extensions.Hosting;
using System.Net;

namespace FileUploadApi.Services
{
    public class PostService : IPostService
    {
        public String? lokasi;
        private readonly SocialDbContext socialDbContext;
        private readonly IWebHostEnvironment environment;

        public PostService(SocialDbContext socialDbContext, IWebHostEnvironment environment)
        {
            this.socialDbContext = socialDbContext;
            this.environment = environment;
        }

        //public async Task<PostResponse> CreatePostAsync(PostRequest postRequest)
        //{
        //    var post = new Entities.Post
        //    {
        //        UserId = postRequest.UserId,
        //        Description = postRequest.Description,
        //        Imagepath = postRequest.Kapal,
        //        TS = DateTime.UtcNow.AddHours(7),

        //        Published = true
        //    };

        //    if (postRequest.Title == "wrong")
        //    {
        //        return new PostResponse { Success = false, Error = "Zip file are corrupt", ErrorCode = "CP04" };


        //    }

        //    var postEntry = await socialDbContext.Post.AddAsync(post);



        //    var saveResponse = await socialDbContext.SaveChangesAsync();

        //    if (saveResponse < 0)
        //    {
        //        return new PostResponse { Success = false, Error = "Issue while saving the post", ErrorCode = "CP01" };
        //    }

        //    var postEntity = postEntry.Entity;
        //    var postModel = new PostModel
        //    {
        //        Id = postEntity.Id,
        //        Description = postEntity.Description,
        //        TS = postEntity.TS,
        //        ZipFile = Path.Combine(postEntity.Imagepath),
        //        UserId = postEntity.UserId,


        //    };




        //    return new PostResponse { Success = true, Post = postModel };



        //}


        public async Task<PostResponse> CreatePostAsync(RawPost rawPost)
        {
            var post = new Entities.RAW
            {
                SUPPLY_CODE = rawPost.SUPPLY_CODE,
                PO_NUMBER  = rawPost.PO_NUMBER,
                SUPPLIER_CODE = rawPost.SUPPLIER_CODE,
                SUPPLIER_NAME= rawPost.SUPPLIER_NAME,
                MFM_SERIALNO= rawPost.MFM_SERIALNO,
                TRANS_DATE= SetKind(rawPost.TRANS_DATE),
                PATH_ =lokasi
            };

            if (lokasi == "wrong")
            {
                return new PostResponse { Success = false, Error = "Zip file are corrupt", ErrorCode = "CP04" };
            }

            var postEntry = await socialDbContext.RAW.AddAsync(post);

            var saveResponse = await socialDbContext.SaveChangesAsync();

            if (saveResponse < 0)
            {
                return new PostResponse { Success = false, Error = "Issue while saving the post", ErrorCode = "CP01" };
            }

            var postEntity = postEntry.Entity;
            var postModel = new rawModel
            {
                model1 = "12334",
                Description = "testing ok"

            };

                return new PostResponse { Success = true, raw = postModel };

        }


        public async Task SavePostImageAsync(RawPost rawPost)
        {
            var uniqueFileName = FileHelper.GetUniqueFileName(rawPost.ZipFile.FileName);

            var uploads = Path.Combine(environment.WebRootPath, "users", "posts", rawPost.SUPPLY_CODE.ToString());

            var filePath = Path.Combine(uploads, uniqueFileName);

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            //await postRequest.ImageFile.CopyToAsync(new FileStream(filePath, FileMode.Create));

            using (var iNeedToLearnAboutDispose = new FileStream(filePath, FileMode.Create))
            {
                await rawPost.ZipFile.CopyToAsync(iNeedToLearnAboutDispose);
            }

            string hash = CheckMD5(filePath);

            if (hash != null && hash == rawPost.ZipHash)
            {
                lokasi = filePath;
                return;


            }
            else
            {

                lokasi = "wrong";
                return;

            }

        }


        


        //public async Task SavePostImageAsync(PostRequest postRequest)
        //{
        //    var uniqueFileName = FileHelper.GetUniqueFileName(postRequest.ZipFile.FileName);
            
        //    var uploads = Path.Combine(environment.WebRootPath, "users", "posts", postRequest.UserId.ToString());

        //    var filePath = Path.Combine(uploads, uniqueFileName);

        //    Directory.CreateDirectory(Path.GetDirectoryName(filePath));

        //    //await postRequest.ImageFile.CopyToAsync(new FileStream(filePath, FileMode.Create));

        //    using (var iNeedToLearnAboutDispose = new FileStream(filePath, FileMode.Create))
        //    {
        //        await postRequest.ZipFile.CopyToAsync(iNeedToLearnAboutDispose);
        //    }

        //    string hash = CheckMD5(filePath);

        //    if (hash != null && hash == postRequest.ZipHash)
        //    {
        //        postRequest.Kapal = filePath;
        //        return;


        //    } else
        //    {

        //        postRequest.Title = "wrong";
        //        return;

        //    }

        //}


        private string CheckMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = System.IO.File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", string.Empty);
                }
            }
        }

        public static DateTime SetKind(DateTime value)
        {
            return DateTime.SpecifyKind(value, DateTimeKind.Utc);
        }


    }
}
