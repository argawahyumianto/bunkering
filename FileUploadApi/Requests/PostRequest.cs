﻿using System.Text.Json.Serialization;

namespace FileUploadApi.Requests
{
    public class PostRequest
    {
        public int UserId { get; set; }
        public string? Description { get; set; }
  
        [JsonIgnore(Condition = JsonIgnoreCondition.Always)]
        public string? Kapal { get; set; }
        public string? Title { get; set; }
        public virtual IFormFile? ZipFile { get; set; }
        public string? ZipHash { get; set; }
    }
}