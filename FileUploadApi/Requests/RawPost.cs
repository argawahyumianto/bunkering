﻿

using System.Text.Json.Serialization;

namespace FileUploadApi.Requests
{
    public class RawPost
    {

        public string? SUPPLY_CODE { get; set; }
        public string? PO_NUMBER { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.Always)]
        public string? SUPPLIER_CODE { get; set; }

        public string? SUPPLIER_NAME { get; set; }
        public string? MFM_SERIALNO { get; set; }

        public DateTime TRANS_DATE { get; set; }

        public virtual IFormFile? ZipFile { get; set; }
        public string? ZipHash { get; set; }

    }
}
