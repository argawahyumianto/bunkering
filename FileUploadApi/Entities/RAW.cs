﻿#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FileUploadApi.Entities
{
    public class RAW
    {
        [Key]
        public string SUPPLY_CODE { get; set; }
        public string PO_NUMBER { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }

        public string MFM_SERIALNO { get; set; }
        public DateTime TRANS_DATE { get; set; }

        public string PATH_ { get; set; }


    }
}
