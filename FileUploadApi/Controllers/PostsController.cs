﻿using FileUploadApi.CloudStorage;
using FileUploadApi.Interfaces;
using FileUploadApi.Requests;
using FileUploadApi.Response;
using FileUploadApi.Response.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace FileUploadApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostsController : ControllerBase
    {
        private readonly ILogger<PostsController> logger;
        private readonly IPostService postService;
        //private readonly ICloudStorage _cloudStorage;

        public PostsController(ILogger<PostsController> logger, IPostService postService)
        {
            this.logger = logger;
            this.postService = postService;
            //this._cloudStorage = cloudStorage;
        }




        //[HttpPost]     
        //[Route("")]
        //[RequestSizeLimit(500000)]
        //[Authorize]
        //public async Task<IActionResult> SubmitPost([FromForm] PostRequest postRequest)
        //{
        //    if (postRequest == null)
        //    {
        //        return BadRequest(new PostResponse { Success = false, ErrorCode = "S01", Error = "Invalid post request" });
        //    }

        //    if (string.IsNullOrEmpty(Request.GetMultipartBoundary()))
        //    {
        //        return BadRequest(new PostResponse { Success = false, ErrorCode = "S02", Error = "Invalid post header" });
        //    }

        //    if (postRequest.ZipFile != null)
        //    {
        //        await postService.SavePostImageAsync(postRequest);
            
        //        //await UploadFile(postRequest);
               
        //    }

        //    var postResponse = await postService.CreatePostAsync(postRequest);

        //    if (!postResponse.Success)
        //    {
        //        return NotFound(postResponse);
        //    }


           
        //    return Ok(postResponse.Post);
        //}


        [HttpPost]
        [Route("")]
        [RequestSizeLimit(50000000)]
        [Authorize]
        public async Task<IActionResult> SubmitPost([FromForm] RawPost rawPost)
        {
            if (rawPost == null)
            {
                return BadRequest(new PostResponse { Success = false, ErrorCode = "S01", Error = "Invalid post request" });
            }

            if (string.IsNullOrEmpty(Request.GetMultipartBoundary()))
            {
                return BadRequest(new PostResponse { Success = false, ErrorCode = "S02", Error = "Invalid post header" });
            }

            if (rawPost.ZipFile != null)
            {
                await postService.SavePostImageAsync(rawPost);

                //await UploadFile(postRequest);

            }

            var postResponse = await postService.CreatePostAsync(rawPost);

            if (!postResponse.Success)
            {
                return NotFound(postResponse);
            }



            return Ok(postResponse.raw);
        }


        //[HttpPost]

        //[Route("upload")]
        //[RequestSizeLimit(500000)]
        //[Authorize]
        //public async Task<IActionResult> SubmitPost([FromForm] RawPost rawPost)
        //{
        //    if (rawPost == null)
        //    {
        //        return BadRequest(new PostResponse { Success = false, ErrorCode = "S01", Error = "Invalid post request" });
        //    }

        //    if (string.IsNullOrEmpty(Request.GetMultipartBoundary()))
        //    {
        //        return BadRequest(new PostResponse { Success = false, ErrorCode = "S02", Error = "Invalid post header" });
        //    }

        //    if (rawPost.ZipFile != null)
        //    {
        //        await postService.SavePostImageAsync(rawPost);

        //        //await UploadFile(postRequest);

        //    }

        //    var postResponse = await postService.CreateSaveData(rawPost,postRequest);

        //    if (!postResponse.Success)
        //    {
        //        return NotFound(postResponse);
        //    }



        //    return Ok(postResponse.raw);
        //}








        //private async Task UploadFile(PostRequest tvShow)
        //{
        //    string fileNameForStorage = FormFileName(tvShow.Title, tvShow.ImageFile.FileName);
        //    //tvShow.ImagePath = await _cloudStorage.UploadFileAsync(tvShow.ImageFile, fileNameForStorage);
        //    tvShow.ImageStorageName = fileNameForStorage;
        //}


        private static string FormFileName(string title, string fileName)
        {
            var fileExtension = Path.GetExtension(fileName);
            var fileNameForStorage = $"{title}-{DateTime.Now.ToString("yyyyMMddHHmmss")}{fileExtension}";

            return fileNameForStorage;
        }





    }
}
