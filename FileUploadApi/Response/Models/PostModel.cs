﻿namespace FileUploadApi.Response.Models
{
    public class PostModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string? Description { get; set; }
        //public string? Imagepath { get; set; }
        public DateTime TS { get; set; }

        //public string? Title { get; set; }
        public string? ZipFile { get; set; }

        //public string? tokenCode { get; set; }
    }
}